package org.cytoscape.centiscape.internal;

public interface Stoppable {
	public boolean isStopped();
}