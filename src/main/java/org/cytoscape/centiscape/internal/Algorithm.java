/**
 * @author scardoni
 */

package org.cytoscape.centiscape.internal; 

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.cytoscape.centiscape.internal.betweenness.BetweennessMethods;
import org.cytoscape.centiscape.internal.betweenness.EdgeBetweenness;
import org.cytoscape.centiscape.internal.betweenness.FinalResultBetweenness;
import org.cytoscape.centiscape.internal.centralities.Centrality;
import org.cytoscape.centiscape.internal.centralities.NodeCentrality;
import org.cytoscape.centiscape.internal.centroid.CentroidMethods;
import org.cytoscape.centiscape.internal.centroid.FinalResultCentroid;
import org.cytoscape.centiscape.internal.closeness.ClosenessMethods;
import org.cytoscape.centiscape.internal.closeness.FinalResultCloseness;
import org.cytoscape.centiscape.internal.degree.Degree;
import org.cytoscape.centiscape.internal.eccentricity.FinalResultEccentricity;
import org.cytoscape.centiscape.internal.eigenVector.CalculateEigenVector;
import org.cytoscape.centiscape.internal.radiality.Radiality;
import org.cytoscape.centiscape.internal.stress.FinalResultStress;
import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;
import org.cytoscape.view.model.CyNetworkView;

public class Algorithm implements Stoppable {
    private boolean stop = false;
    private List<FinalResultEccentricity> eccentricityResults;
    private List<FinalResultBetweenness> betweennessResults;
    private List<FinalResultCentroid> centroidResults;
    private List<Long> centroidNodeSUIDs;
    private double[][] adjacencyMatrixOfNetwork;
    private boolean StressisOn = false;
    private boolean ClosenessisOn = false;
    private boolean EccentricityisOn = false;
    private boolean RadialityisOn = false;
    private boolean diameterIsSelected = false;
    private boolean betweennessIsOn = false;
    private boolean CentroidisOn = false;
    private boolean DegreeisOn = false;
    private boolean AverageisOn = false;
    private boolean EigenVectorisOn = false;
    private boolean BridgingisOn = false;
    private boolean EdgeBetweennessisOn = false;
    private boolean doNotDisplayBetweenness = false;
    private SortedMap<Long, Double> stressMap = new TreeMap<Long, Double>();
    private List<String> nodeAttributes = new ArrayList<String>();
    private List<String> networkAttributes = new ArrayList<String>();
    private final List<Centrality> centralities = new ArrayList<Centrality>();
    private CyNetwork network;
    private final Core core;
    private List<CyNode> nodeList;
	private CyTable nodeTable;
	private CyTable networkTable;
	private ShortestPaths shortestPaths;

    public Algorithm(Core core) {
        this.core = core; 
    }

    public void execute(CyNetwork network, CyNetworkView view, CentiScaPeStartMenu menu) throws AlgorithmInterruptedException {
    	this.stop = false;
        this.network = network;
        this.nodeTable = network.getDefaultNodeTable();
        this.networkTable = network.getDefaultNetworkTable();
        this.nodeList = network.getNodeList();

        int nodesCount = network.getNodeCount();

        removeCentralitiesFromPanel();

        eccentricityResults = new ArrayList<FinalResultEccentricity>();
        centroidResults = new ArrayList<FinalResultCentroid>();
        betweennessResults = new ArrayList<FinalResultBetweenness>();
        centroidNodeSUIDs = new ArrayList<Long>();
        centralities.clear();

        if (EigenVectorisOn)
            adjacencyMatrixOfNetwork = new double[nodesCount][nodesCount];

        if (betweennessIsOn || StressisOn || CentroidisOn) {
            stressMap.clear();
            
            for (CyNode root: nodeList) {
            	checkForStop();

                if (StressisOn || betweennessIsOn)
                    stressMap.put(root.getSUID(), 0.0);
                if (CentroidisOn)
                    centroidNodeSUIDs.add(root.getSUID());
            }
        }

        shortestPaths = new ShortestPaths(network, stressMap, StressisOn, false, menu, this);

        unselectAllNodes();

        if (CentroidisOn) {
        	for (CyNode root: nodeList)
                CentroidMethods.updateCentroid(shortestPaths.getReducedFor(root), root, nodesCount, centroidNodeSUIDs, centroidResults);
            CentroidMethods.computeCentroid(centroidResults, nodesCount, centroidNodeSUIDs);
        }

        menu.endOfComputation(nodesCount);

        if (diameterIsSelected) {
            networkTable.createColumn("Diameter unDir", Double.class, false);
            network.getRow(network).set("Diameter unDir", (double) shortestPaths.getDiameter());

            networkAttributes.add("Diameter unDir");
        }

        if (AverageisOn) {
        	int distance = 0;
	        for (CyNode root: nodeList)
	        	for (ShortestPathList currentlist: shortestPaths.getReducedFor(root))
	                distance += currentlist.getCost();

	        double average = ((double) distance) / (nodesCount * (nodesCount - 1));
            networkTable.createColumn("Average Distance unDir", Double.class, false);
            network.getRow(network).set("Average Distance unDir", average);

            networkAttributes.add("Average Distance unDir");
        }

        if (EccentricityisOn) {
        	for (CyNode root: nodeList)
        		eccentricityResults.add(new FinalResultEccentricity(root, shortestPaths.getReducedFor(root)));

        	nodeTable.createColumn("Eccentricity unDir", Double.class, false);
            nodeAttributes.add("Eccentricity unDir");
            double min = Double.MAX_VALUE, max = -Double.MAX_VALUE, totalsum = 0;
            for (FinalResultEccentricity result: eccentricityResults) {
                double currenteccentricity = result.getEccentricity();
                if (currenteccentricity < min)
                    min = currenteccentricity;

                if (currenteccentricity > max)
                    max = currenteccentricity;

                totalsum = totalsum + currenteccentricity;
                CyRow row = nodeTable.getRow(result.getNode().getSUID());
                row.set("Eccentricity unDir", new Double(currenteccentricity));
            }
            networkTable.createColumn("Eccentricity Max value unDir", Double.class, false);
            networkTable.createColumn("Eccentricity min value unDir", Double.class, false);
            double mean = totalsum / nodesCount;
            networkTable.createColumn("Eccentricity mean value unDir", Double.class, false);
            network.getRow(network).set("Eccentricity Max value unDir", new Double(max));
            network.getRow(network).set("Eccentricity min value unDir", new Double(min));
            network.getRow(network).set("Eccentricity mean value unDir", new Double(mean));

            networkAttributes.add("Eccentricity Max value");
            networkAttributes.add("Eccentricity min value");
            networkAttributes.add("Eccentricity mean value");

            NodeCentrality eccentricityCentrality = new NodeCentrality("Eccentricity unDir", mean, min, max);
            centralities.add(eccentricityCentrality);
        }

        if (ClosenessisOn) {
        	List<FinalResultCloseness> closenessResults = new ArrayList<FinalResultCloseness>();
        	for (CyNode root: nodeList)
        		closenessResults.add(ClosenessMethods.CalculateCloseness(shortestPaths.getReducedFor(root), root));

        	nodeTable.createColumn("Closeness unDir", Double.class, false);
            nodeAttributes.add("Closeness unDir");
            double min = Double.MAX_VALUE, max = -Double.MAX_VALUE, totalsum = 0;
            for (FinalResultCloseness result: closenessResults) {
                double currentcloseness = result.getCloseness();
                if (currentcloseness < min)
                    min = currentcloseness;

                if (currentcloseness > max)
                    max = currentcloseness;

                totalsum = totalsum + currentcloseness;

                CyRow row = nodeTable.getRow(result.getNode().getSUID());
                row.set("Closeness unDir", new Double(currentcloseness));
            }
            networkTable.createColumn("Closeness Max value unDir", Double.class, false);
            networkTable.createColumn("Closeness min value unDir", Double.class, false);
            double mean = totalsum / nodesCount;
            networkTable.createColumn("Closeness mean value unDir", Double.class, false);
            network.getRow(network).set("Closeness Max value unDir", new Double(max));
            network.getRow(network).set("Closeness min value unDir", new Double(min));
            network.getRow(network).set("Closeness mean value unDir", new Double(mean));
            networkAttributes.add("Closeness Max value");
            networkAttributes.add("Closeness min value");
            networkAttributes.add("Closeness mean value");
            centralities.add(new NodeCentrality("Closeness unDir", mean, min, max));
        }

        if (RadialityisOn) {
        	Radiality radiality = new Radiality(shortestPaths);
            radiality.showInPanelFor(network);
            centralities.add(radiality);
        }

        if (betweennessIsOn) {
        	for (CyNode root: nodeList)
        		betweennessResults.add(new FinalResultBetweenness(root, 0));

        	for (CyNode root: nodeList)
        		BetweennessMethods.updateBetweenness(shortestPaths.getFor(root), betweennessResults);

        	if (!doNotDisplayBetweenness) {
	        	nodeTable.createColumn("Betweenness unDir", Double.class, false);
	            nodeAttributes.add("Betweenness unDir");
	            double min = Double.MAX_VALUE, max = -Double.MAX_VALUE, totalsum = 0;
	
	            for (FinalResultBetweenness result: betweennessResults) {
	                double currentbetweenness = result.getBetweenness();
	
	                if (currentbetweenness < min)
	                    min = currentbetweenness;
	
	                if (currentbetweenness > max)
	                    max = currentbetweenness;
	
	                totalsum = totalsum + currentbetweenness;
	                CyRow row = nodeTable.getRow(result.getNode().getSUID());
	                row.set("Betweenness unDir", new Double(currentbetweenness));
	            }
	            networkTable.createColumn("Betweenness Max value unDir", Double.class, false);
	            networkTable.createColumn("Betweenness min value unDir", Double.class, false);
	            double mean = totalsum / nodesCount;
	            networkTable.createColumn("Betweenness mean value unDir", Double.class, false);
	            network.getRow(network).set("Betweenness Max value unDir", new Double(max));
	            network.getRow(network).set("Betweenness min value unDir", new Double(min));
	            network.getRow(network).set("Betweenness mean value unDir", new Double(mean));
	            networkAttributes.add("Betweenness Max value");
	            networkAttributes.add("Betweenness min value");
	            networkAttributes.add("Betweenness mean value");
	            NodeCentrality betweennessCentrality = new NodeCentrality("Betweenness unDir", mean, min, max);
	            centralities.add(betweennessCentrality);
        	}
        }

        if (DegreeisOn) {
        	Degree degree = new Degree(network);
        	degree.showInPanelFor(network);
            centralities.add(degree);
        }

        if (StressisOn) {
            nodeTable.createColumn("Stress unDir", Double.class, false);
            nodeAttributes.add("Stress unDir");

            double min = Double.MAX_VALUE, max = -Double.MAX_VALUE, totalsum = 0;
            List<FinalResultStress> stressResults = new ArrayList<FinalResultStress>();
            for (long nodeSUID: stressMap.keySet()) {
                CyNode node = network.getNode(nodeSUID);
                double stress = stressMap.get(nodeSUID);
                stressResults.add(new FinalResultStress(node, stress));

                min = Math.min(min, stress);
                max = Math.max(max, stress);
                totalsum += stress;

                CyRow row = nodeTable.getRow(nodeSUID);
                row.set("Stress unDir", stress);
            }
            networkTable.createColumn("Stress Max value unDir", Double.class, false);
            networkTable.createColumn("Stress min value unDir", Double.class, false);
            double mean = totalsum / nodesCount;
            networkTable.createColumn("Stress mean value unDir", Double.class, false);
            network.getRow(network).set("Stress Max value unDir", new Double(max));
            network.getRow(network).set("Stress min value unDir", new Double(min));
            network.getRow(network).set("Stress mean value unDir", new Double(mean));
            networkAttributes.add("Stress Max value");
            networkAttributes.add("Stress min value");
            networkAttributes.add("Stress mean value");
            NodeCentrality stressCentrality = new NodeCentrality("Stress unDir", mean, min, max);
            centralities.add(stressCentrality);
        }
        if (CentroidisOn) {
            nodeTable.createColumn("Centroid unDir", Double.class, false);
            nodeAttributes.add("Centroid unDir");
            double min = Double.MAX_VALUE, max = -Double.MAX_VALUE, totalsum = 0;
            for (FinalResultCentroid result: centroidResults) {
                double currentcentroid = result.getCentroid();

                if (currentcentroid < min)
                    min = currentcentroid;

                if (currentcentroid > max)
                    max = currentcentroid;

                totalsum = totalsum + currentcentroid;
                CyRow row = nodeTable.getRow(result.getNode().getSUID());
                row.set("Centroid unDir", new Double(currentcentroid));
            }
            networkTable.createColumn("Centroid Max value unDir", Double.class, false);
            networkTable.createColumn("Centroid min value unDir", Double.class, false);
            double mean = totalsum / nodesCount;
            networkTable.createColumn("Centroid mean value unDir", Double.class, false);
            network.getRow(network).set("Centroid Max value unDir", new Double(max));
            network.getRow(network).set("Centroid min value unDir", new Double(min));
            network.getRow(network).set("Centroid mean value unDir", new Double(mean));
      
            networkAttributes.add("Centroid Max value unDir");
            networkAttributes.add("Centroid min value unDir");
            networkAttributes.add("Centroid mean value unDir");
            centralities.add(new NodeCentrality("Centroid unDir", mean, min, max));
        }
        if (EigenVectorisOn) {
        	int k = 0;
        	for (CyNode root: nodeList) {
                List<CyNode> neighbors = network.getNeighborList(root, CyEdge.Type.ANY);
                for(CyNode neighbor: neighbors)
                    adjacencyMatrixOfNetwork[k][nodeList.indexOf(neighbor)] = 1.0 ;
                k++;
            }
            nodeAttributes.add("EigenVector unDir");   
            networkAttributes.add("EigenVector Max value unDir");
            networkAttributes.add("EigenVector min value unDir");
            networkAttributes.add("EigenVector mean value unDir");
            
            CalculateEigenVector.executeAndWriteValues(adjacencyMatrixOfNetwork, network, "EigenVector ", centralities, "unDir");
        }
        if (BridgingisOn) {
            nodeTable.createColumn("Bridging unDir", Double.class, false);
            nodeAttributes.add("Bridging unDir");
            double min = Double.MAX_VALUE, max = -Double.MAX_VALUE, totalsum = 0;

            for (FinalResultBetweenness result: betweennessResults) {
            	double currentbetweenness = result.getBetweenness();
            	CyNode root = result.getNode();
            	List<CyNode> bridgingNeighborList = network.getNeighborList(root, CyEdge.Type.ANY);
            	double bridgingCoefficient = 0;if(bridgingNeighborList.size() != 0){
            		double BCNumerator = 1.0 / bridgingNeighborList.size();
            		double BCDenominator = 0.0;
            		for (CyNode bridgingNeighbor: bridgingNeighborList)
            			BCDenominator = BCDenominator + 1/(double)(network.getNeighborList(bridgingNeighbor, CyEdge.Type.ANY).size());

            		bridgingCoefficient = BCNumerator/BCDenominator;
            	}
            	double bridgingCentrality = bridgingCoefficient*currentbetweenness;
            	if ( bridgingCentrality < min)
            		min = bridgingCentrality;

            	if (bridgingCentrality > max)
            		max = bridgingCentrality;

            	totalsum = totalsum + bridgingCentrality;
            	CyRow row = nodeTable.getRow(result.getNode().getSUID());
            	row.set("Bridging unDir", new Double(bridgingCentrality));
            }
            networkTable.createColumn("Bridging Max value unDir", Double.class, false);
            networkTable.createColumn("Bridging min value unDir", Double.class, false);
            double mean = totalsum / nodesCount;
            networkTable.createColumn("Bridging mean value unDir", Double.class, false);
            network.getRow(network).set("Bridging Max value unDir", new Double(max));
            network.getRow(network).set("Bridging min value unDir", new Double(min));
            network.getRow(network).set("Bridging mean value unDir", new Double(mean));
            networkAttributes.add("Bridging Max value unDir");
            networkAttributes.add("Bridging min value unDir");
            networkAttributes.add("Bridging mean value unDir");
            centralities.add(new NodeCentrality("Bridging unDir", mean, min, max));
        }
        if (EdgeBetweennessisOn) {
            EdgeBetweenness edgeBetweenness = new EdgeBetweenness(network, shortestPaths);
            edgeBetweenness.showInPanelFor(network);
            centralities.add(edgeBetweenness);
        }
        core.createVisualizer(centralities);
    }

	public void end() {
        stop = true;
    }

    public void setChecked(boolean[] ison) {
        diameterIsSelected = ison[0];
        AverageisOn = ison[1];
        DegreeisOn = ison[2];
        EccentricityisOn = ison[3];
        RadialityisOn = ison[4];
        ClosenessisOn = ison[5];
        StressisOn = ison[6];
        betweennessIsOn = ison[7];
        CentroidisOn = ison[8];
        EigenVectorisOn = ison[9];
        BridgingisOn = ison[10];
        EdgeBetweennessisOn = ison[11];
        if (BridgingisOn && !betweennessIsOn){
            betweennessIsOn = true;
            doNotDisplayBetweenness = true;
        }
        if(ison[7])
            doNotDisplayBetweenness = false;
    }

    private void checkForStop() throws AlgorithmInterruptedException {
		if (stop) {
	        unselectAllNodes();
	        throw new AlgorithmInterruptedException();
	    }
	}

	private void unselectAllNodes() {
		//TODO lo chiamerei select all nodes!
        for (CyNode node: nodeList)
        	network.getRow(node).set("selected", true);
    }

    private void removeCentralitiesFromPanel() {
    	for (Centrality centrality: centralities)
    		centrality.removeFromPanel(network);

    	for (String attribute: nodeAttributes)
	        if (nodeTable.getColumn(attribute) != null)
	            nodeTable.deleteColumn(attribute);
	
	    for (String attribute: networkAttributes)
	        if (networkTable.getColumn(attribute) != null)
	        	networkTable.deleteColumn(attribute);
	}

	@Override
	public boolean isStopped() {
		return stop;
	}
}