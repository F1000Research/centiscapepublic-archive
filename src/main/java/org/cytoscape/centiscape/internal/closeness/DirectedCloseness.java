package org.cytoscape.centiscape.internal.closeness;

import java.util.List;
import org.cytoscape.centiscape.internal.ShortestPathList;

/**
 * @author faizi
 */

public class DirectedCloseness {

	public static double execute(List<ShortestPathList> shortestPaths){
		double closeness = 0;
		for (ShortestPathList path: shortestPaths)
			// TODO: e se il costo fosse zero? non è numericamente più stabile sommare i costi e poi restituire 1.0 / somma ?
			closeness += 1.0 / path.getCost();

		return closeness;
	}
}